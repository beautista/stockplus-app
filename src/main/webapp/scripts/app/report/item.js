/**
 * 
 */

var object = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}



function searchReport(){
	var criteriaObject = {
			startDate    : $("#search_startDate").val()==""?"null":$("#search_startDate").val(),
			endDate    : $("#search_endDate").val()==""?"null":$("#search_endDate").val(),
			itemCode    : $("#search_itemCode").val()==""?"null":$("#search_itemCode").val(),
			itemBarcode    : $("#search_itemBarcode").val()==""?"null":$("#search_itemBarcode").val(),
			itemName    : $("#search_itemName").val()==""?"null":$("#search_itemName").val()
							
	    };
	queryReportByCriteria(criteriaObject);
}

function queryReportByCriteria(criteriaObject){
	object.setId("#paggingReport");                
    object.setUrlData("/report/item/findByCriteria"); 
    object.setUrlSize("/report/item/findSize"); 
    
    object.loadTable = function(item){
        $('.dv-background').show();
        $('#gridReportBody').empty();
        $dataForQuery = item;
        for(var j=0;j<item.length;j++){
            var itemCode = item[j].itemCode==null?"":item[j].itemCode;
            var itemBarcode = item[j].itemBarcode==null?"":item[j].itemBarcode;
            var itemName = item[j].itemName==null?"":item[j].itemName;
            var quantity = item[j].quantity==null?"":item[j].quantity;
            var stockOnhand = item[j].stockOnhand==null?"":item[j].stockOnhand;
            var minimumStock = item[j].minimumStock==null?"":item[j].minimumStock;
            
            
            
            $('#gridReportBody').append(''+
                    '<tr id="'+item[j].id+'" '+
                    ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                    '<td class="text-center" >'+itemCode+'</td>'+
                    '<td class="text-center" >'+itemBarcode+'</td>'+
                    '<td class="text-center" >'+itemName+'</td>'+
                    '<td class="text-center" >'+quantity+'</td>'+
                    '<td class="text-center" >'+stockOnhand+'</td>'+
                    '<td class="text-center" >'+minimumStock+'</td>'+
                    '</tr>'
                );
        }
        $('.dv-background').hide();
    };

    object.setDataSearch(criteriaObject); 

    object.search(object); 
}









$(document).ready(function () {
	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_button').on('click', function () {
		searchReport();
	});
	
	
	
	
	searchReport();
	
});

