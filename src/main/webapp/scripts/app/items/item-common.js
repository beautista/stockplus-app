/**
 * 
 */

var object = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}

function calculateSellingPrice(){
	//Have Promotion Price Amount is main
	if(!isZero('#modal_edit_normal_selling_price_amt') && 
			!isZero('#modal_edit_promotion_selling_price_pct')){
		var promotion_selling_price_amt = ($('#modal_edit_normal_selling_price_amt').val() * (100 - $('#modal_edit_promotion_selling_price_pct').val()))/100;
		promotion_selling_price_amt = parseFloat(promotion_selling_price_amt).toFixed(2)
		$('#modal_edit_promotion_selling_price_amt').val(promotion_selling_price_amt);
		$('#modal_edit_selling_price').html(promotion_selling_price_amt);
	}else if(!isZero('#modal_edit_promotion_selling_price_amt')){
		$('#modal_edit_selling_price').html($('#modal_edit_promotion_selling_price_amt').val());
	}else if(!isZero('#modal_edit_normal_selling_price_amt') && isZero('#modal_edit_promotion_selling_price_pct')){
		$('#modal_edit_selling_price').html($('#modal_edit_normal_selling_price_amt').val());
	}else if(!isZero('#modal_edit_normal_selling_price_amt') || isZero('#modal_edit_promotion_selling_price_amt')){
		$('#modal_edit_selling_price').html('0.00');
	}
}

function calculatePurchasePrice(){
	if(!isZero('#modal_edit_normal_purchase_price_amt') && 
			!isZero('#modal_edit_promotion_purchase_price_pct')){
		var promotion_purchase_price_amt = ($('#modal_edit_normal_purchase_price_amt').val() * (100 - $('#modal_edit_promotion_purchase_price_pct').val()))/100;
		promotion_purchase_price_amt = parseFloat(promotion_purchase_price_amt).toFixed(2)
		$('#modal_edit_promotion_purchase_price_amt').val(promotion_purchase_price_amt);
		$('#modal_edit_purchase_price').html(promotion_purchase_price_amt);
	}else if(!isZero('#modal_edit_promotion_purchase_price_amt')){
		$('#modal_edit_purchase_price').html($('#modal_edit_promotion_purchase_price_amt').val());
	}else if(!isZero('#modal_edit_normal_purchase_price_amt') && isZero('#modal_edit_promotion_purchase_price_pct')){
		$('#modal_edit_purchase_price').html($('#modal_edit_normal_purchase_price_amt').val());
	}else if(!isZero('#modal_edit_normal_purchase_price_amt') || isZero('#modal_edit_promotion_purchase_price_amt')){
		$('#modal_edit_purchase_price').html('0.00');
	}
}

function calculateContribute(){
	var cont_amt = $('#modal_edit_selling_price').html() - $('#modal_edit_purchase_price').html();
	cont_amt = setZeroWhenNull(cont_amt);
	$('#modal_edit_cont_amt').html(parseFloat(cont_amt).toFixed(2));
	
	var cont_pct = (($('#modal_edit_selling_price').html() - $('#modal_edit_purchase_price').html()) / $('#modal_edit_selling_price').html()) * 100 ;
	cont_pct = setZeroWhenNull(cont_pct);
	$('#modal_edit_cont_pct').html(parseFloat(cont_pct).toFixed(2));
}

function calculateMagin(){
	var fix_cost_amt = ($('#modal_edit_fix_cost_pct').html()/100) * $('#modal_edit_selling_price').html();
	fix_cost_amt = setZeroWhenNull(fix_cost_amt);
	
	var margin_amt = $('#modal_edit_selling_price').html() - $('#modal_edit_purchase_price').html() - fix_cost_amt;
	margin_amt = setZeroWhenNull(margin_amt);
	$('#modal_edit_margin_amt').html(parseFloat(margin_amt).toFixed(2));
	
	var margin_pct = (($('#modal_edit_selling_price').html() - $('#modal_edit_purchase_price').html()  - fix_cost_amt) / $('#modal_edit_selling_price').html()) * 100 ;
	margin_pct = setZeroWhenNull(margin_pct);
	$('#modal_edit_margin_pct').html(parseFloat(margin_pct).toFixed(2));
}

function calculateContributeShow(){
	var cont_amt = $('#show_selling_price').html() - $('#show_purchase_price').html();
	cont_amt = setZeroWhenNull(cont_amt);
	$('#show_cont_amt').html(parseFloat(cont_amt).toFixed(2));
	
	var cont_pct = (($('#show_selling_price').html() - $('#show_purchase_price').html()) / $('#show_selling_price').html()) * 100 ;
	cont_pct = setZeroWhenNull(cont_pct);
	$('#show_cont_pct').html(parseFloat(cont_pct).toFixed(2));
}

function calculateMarginShow(){
	var fix_cost_amt = ($('#show_fix_cost_pct').html()/100) * $('#show_selling_price').html();
	fix_cost_amt = setZeroWhenNull(fix_cost_amt);
	
	var margin_amt = $('#show_selling_price').html() - $('#show_purchase_price').html() - fix_cost_amt;
	margin_amt = setZeroWhenNull(margin_amt);
	$('#show_margin_amt').html(parseFloat(margin_amt).toFixed(2));
	
	var margin_pct = (($('#show_selling_price').html() - $('#show_purchase_price').html()  - fix_cost_amt) / $('#show_selling_price').html()) * 100 ;
	margin_pct = setZeroWhenNull(margin_pct);
	$('#show_margin_pct').html(parseFloat(margin_pct).toFixed(2));
}



function showDataItem(item_id){
	var itemData = $.ajax({
        type: "GET",
        url: session['context']+'/items/'+item_id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	$("#collapseTwo").collapse('show');
                	
                	$("tr").removeClass("trblue");
                    var item = JSON.parse(xhr.responseText);
                    if(item.stockOnHand){
                    	$("#show_stockOnHand").html(item.stockOnHand);
                    }else{
                    	$("#show_stockOnHand").html('0');
                    }
                    
                    if(item.minimumStock){
                    	$("#show_minimumStock").html(item.minimumStock);
                    }else{
                    	$("#show_minimumStock").html('0');
                    }
                    
                    $("#show_selling_price").html(item.sellingPriceAmount);
                    $("#show_purchase_price").html(item.purchasePriceAmount);
                    $("#show_normal_selling_price_amt").val(item.normalSellingPriceAmount);
                    $("#show_promotion_selling_price_amt").val(item.promotionSellingPriceAmount);
                    $("#show_promotion_selling_price_pct").val(item.promotionSellingPricePercentage);
                    $("#show_normal_purchase_price_amt").val(item.normalPurchasePriceAmount);
                    $("#show_promotion_purchase_price_amt").val(item.promotionPurchasePriceAmount);
                    $("#show_promotion_purchase_price_pct").val(item.promotionPurchasePricePercentage);
                    
                    calculateContributeShow();
                    calculateMarginShow();
                    $('#search_barcode').focus();
                    $('#'+item_id).addClass('trblue');
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function searchItem(){
	var criteriaObject = {
	        itemCode    : $("#search_itemCode").val()==""?"null":$("#search_itemCode").val(),
	        barcode     : $("#search_barcode").val()==""?"null":$("#search_barcode").val(),
	        name        : $("#search_itemCName").val()==""?"null":$("#search_itemCName").val()
	    };
	queryItemByCriteria(criteriaObject);
}

function queryItemByCriteria(criteriaObject){
	object.setId("#paggingSearchItem");                
    object.setUrlData("/items/findItemByCriteria"); 
    object.setUrlSize("/items/findItemSize");    
    
    object.loadTable = function(item){
        $('.dv-background').show();
        $('#gridItemBody').empty();
        
        if(item.length > 0){
        	$dataForQuery = item;
            var itemIdTmp ;
            for(var j=0;j<item.length;j++){
                var itemCode = item[j].itemCode==null?"":item[j].itemCode;
                var itemBarcode = item[j].itemBarcode==null?"":item[j].itemBarcode;
                var name = item[j].name==null?"":item[j].name;
                var stockOnHand = item[j].stockOnHand==null?"":item[j].stockOnHand;
                var sellingPriceAmount = item[j].sellingPriceAmount==null?"":item[j].sellingPriceAmount;
                itemIdTmp = item[j].id;
                $('#gridItemBody').append(''+
                        '<tr id="'+item[j].id+'" '+
                        ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                        '<td class="text-center">'+itemCode+'</td>'+
                        '<td class="text-center">'+itemBarcode+'</td>'+
                        '<td class="text-center">'+name+'</td>'+
                        '<td class="text-center">'+stockOnHand+'</td>'+
                        '<td class="text-center">'+sellingPriceAmount+'</td>'+
                        '<td class="text-center">'+
                        '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal" data-target="#editItemModal" onclick="editItem('+item[j].id+')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
        				'	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+item[j].id+'\');$(\'#codeItemDelete\').html(\''+itemCode+'\');"><span class="fa fa-trash"><jsp:text/></span></button>'+
        				'	<button type="button"  class="btn btn-material-cyan-500  btn-style-small" title="View" onclick="showDataItem('+item[j].id+')" ><span class="fa fa-arrow-right"><jsp:text/></span></button>'+
        				'</td>'+
                        '</tr>'
                    );
            }
            
            if(item.length==1){
            	showDataItem(itemIdTmp);
            	$('#search_barcode').val('');
            }
            
            $('.dv-background').hide();
            clearFormShowData();
            $('#search_barcode').focus();
            pressed = false;
        }else{
        	$('#warningModal .modal-body').html('Barcode not found item.');
			$('#warningModal').modal('show');
			$('#search_barcode').val('');
			//searchItem();
        }
        
    };

    object.setDataSearch(criteriaObject); 

    object.search(object); 
}

function editItem(item_id){
	var itemData = $.ajax({
        type: "GET",
        url: session['context']+'/items/'+item_id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var item = JSON.parse(xhr.responseText);
                    $('#modal_edit_item_code').attr('disabled', true);
                    
                    $('#modal_edit_id').val(item.id);
                    $('#modal_edit_item_code').val(item.itemCode);
                    $('#modal_edit_barcode').val(item.itemBarcode);
                    $('#modal_edit_name').val(item.name);
                    $("#modal_edit_stock_on_hand").val(item.stockOnHand);
                    $("#modal_edit_minimum_stock").val(item.minimumStock);
                    $('#modal_edit_selling_price').html(parseFloat(item.sellingPriceAmount).toFixed(2));
                	$('#modal_edit_normal_selling_price_amt').val(parseFloat(item.normalSellingPriceAmount).toFixed(2));
                	$('#modal_edit_promotion_selling_price_amt').val(parseFloat(item.promotionSellingPriceAmount).toFixed(2));
                	$('#modal_edit_promotion_selling_price_pct').val(item.promotionSellingPricePercentage);
                	$('#modal_edit_purchase_price').html(parseFloat(item.purchasePriceAmount).toFixed(2));
                	$('#modal_edit_normal_purchase_price_amt').val(parseFloat(item.normalPurchasePriceAmount).toFixed(2));
                	$('#modal_edit_promotion_purchase_price_amt').val(parseFloat(item.promotionPurchasePriceAmount).toFixed(2));
                	$('#modal_edit_promotion_purchase_price_pct').val(item.promotionPurchasePricePercentage);
                	$('#modal_edit_createdBy').val(item.createdBy);
                	$('#modal_edit_createdDate').val(item.createdDate);
                	$('#modal_edit_updateBy').val(item.updateBy);
                	$('#modal_edit_updateDate').val(item.updateDate);
                	    		
                	calculateSellingPrice();
            		calculateContribute();
            		calculateMagin();
                }
            }
        }
    }).done(function (){
        //close loader
    });
	
}

function clearFormShowData(){
	$('#show_stockOnHand').html('-');
	$('#show_minimumStock').html('-');
	
	
	$('#show_selling_price').html('0.00');
	$('#show_normal_selling_price_amt').val('');
	$('#show_promotion_selling_price_amt').val('');
	$('#show_promotion_selling_price_pct').val('');
	
	$('#show_purchase_price').html('0.00');
	$('#show_normal_purchase_price_amt').val('');
	$('#show_promotion_purchase_price_amt').val('');
	$('#show_promotion_purchase_price_pct').val('');
	
	$('#show_cont_amt').html(parseFloat(0).toFixed(2));
	$('#show_cont_pct').html(parseFloat(0).toFixed(2));
	$('#show_margin_amt').html(parseFloat(0).toFixed(2));
	$('#show_margin_pct').html(parseFloat(0).toFixed(2));
}

function deleteItem(item_id){
    var jsonParams = {};
	
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	
	var itemData = $.ajax({
        type: "DELETE",
        url: session['context']+'/items/'+item_id,
        data: jsonParams,
        complete: function (xhr) {
        	searchItem();
        	$('#deleteItemModal').modal('hide');
        }
    }).done(function (){
        //close loader
    });
}

function saveItem(){
	var jsonParams = {};
	
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	jsonParams['id']     = $('#modal_edit_id').val();
	jsonParams['itemCode']     = $('#modal_edit_item_code').val();
	jsonParams['itemBarcode']  = $('#modal_edit_barcode').val();
	jsonParams['name']         = $('#modal_edit_name').val();
	jsonParams['stockOnHand']  = $('#modal_edit_stock_on_hand').val();
	jsonParams['minimumStock'] = $('#modal_edit_minimum_stock').val();
	jsonParams['sellingPriceAmount'] = $('#modal_edit_selling_price').html();
	jsonParams['normalSellingPriceAmount'] = $('#modal_edit_normal_selling_price_amt').val();
	jsonParams['promotionSellingPriceAmount'] = $('#modal_edit_promotion_selling_price_amt').val();
	jsonParams['promotionSellingPricePercentage'] = $('#modal_edit_promotion_selling_price_pct').val();
	jsonParams['purchasePriceAmount'] = $('#modal_edit_purchase_price').html();
	jsonParams['normalPurchasePriceAmount'] = $('#modal_edit_normal_purchase_price_amt').val();
	jsonParams['promotionPurchasePriceAmount'] = $('#modal_edit_promotion_purchase_price_amt').val();
	jsonParams['promotionPurchasePricePercentage'] = $('#modal_edit_promotion_purchase_price_pct').val();
	jsonParams['createdBy'] = $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] = $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] = $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] = $('#modal_edit_updateDate').val();
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/items/save',
        data: jsonParams,
        complete: function (xhr) {
        	searchItem();
        	$('#editItemModal').modal('hide');
        }
    }).done(function (){
        //close loader
    });
}

var timeDifKeyPress = 0.0;
var pressed = false; 
var stompClient = null;


function connect() {
	
    var socket = new SockJS('/StockPlus/barcodescan');
    stompClient = Stomp.over(socket);  
    
    stompClient.connect({}, function(frame) {
        
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/barcodescan/messages', function(messageOutput) {
        	var messageObj = JSON.parse(messageOutput.body);
        	$('#search_barcode').val(messageObj.text);
        	searchItem();
        });
    });
}


$(document).ready(function () {
	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_barcode').focus();
	
	$('#search_barcode').on("keyup",function(e){
		var difTime = e.originalEvent.timeStamp - timeDifKeyPress;
		
		timeDifKeyPress = e.originalEvent.timeStamp;
		
		//difTime < 40 && 
		if ($('#search_barcode').val().length >= 13 && pressed==false) {
			//Use for case more EAN13
			/*if(pressed == false){
				console.log("before callSaveItemFromBarcodeScanner");
				callSaveItemFromBarcodeScanner();
			}
			*/
			pressed = true;
			
			searchItem();
		}
		
		
    });

	
	$('#modal_edit_normal_selling_price_amt,#modal_edit_promotion_selling_price_amt,#modal_edit_promotion_selling_price_pct').on("change",function(e){
		calculateSellingPrice();
		calculateContribute();
		calculateMagin();
		
    });
	
	
	$('#modal_edit_normal_purchase_price_amt,#modal_edit_promotion_purchase_price_amt,#modal_edit_promotion_purchase_price_pct').on("change",function(e){
		calculatePurchasePrice();
		calculateContribute();
		calculateMagin();
    });
	

	
	
	
	$('#warningModal').on('hidden.bs.modal', function () {
		$('#search_barcode').focus();
	});
	
	$('#editItemModal').on('shown.bs.modal', function () {
		
		$('#modal_edit_barcode').focus();
	});
	
	$('#editItemModal').on('hidden.bs.modal', function () {

        $('#modal_edit_item_code').attr('disabled', false);
		$('#modal_edit_id').val('');
		$('#modal_edit_item_code').val('');
		$('#modal_edit_barcode').val('');
		$('#modal_edit_name').val('');
		$('#modal_edit_stock_on_hand').val('');
		$('#modal_edit_minimum_stock').val('');
		
		
		$('#modal_edit_selling_price').html('0.00');
		$('#modal_edit_normal_selling_price_amt').val('');
		$('#modal_edit_promotion_selling_price_amt').val('');
		$('#modal_edit_promotion_selling_price_pct').val('');
		
		$('#modal_edit_purchase_price').html('0.00');
		$('#modal_edit_normal_purchase_price_amt').val('');
		$('#modal_edit_promotion_purchase_price_amt').val('');
		$('#modal_edit_promotion_purchase_price_pct').val('');
		
		$('#modal_edit_cont_amt').html(parseFloat(0).toFixed(2));
		$('#modal_edit_cont_pct').html(parseFloat(0).toFixed(2));
		$('#modal_edit_margin_amt').html(parseFloat(0).toFixed(2));
		$('#modal_edit_margin_pct').html(parseFloat(0).toFixed(2));
	});
	
	$('#search_button').on('click', function () {
		searchItem();
	});
	
	
	$('#editItemModalSave').on('click', function () {
		//validate Mandatory
		var errorMessage = "";
		
		if(!$('#modal_edit_name').val()){
			errorMessage += $MESSAGE_REQUIRE_PRODUCT_NAME+"<br/>";
		}
		
		
		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			saveItem();
		}
		
	});
	
	
	
	searchItem();
	connect();
});

