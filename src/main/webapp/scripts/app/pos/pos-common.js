/**
 * 
 */

var object = $.extend({},UtilPagination);
var object2 = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function clearPosList(){
	var saler =  $("#app_username").html()==""?"null":$("#app_username").html() ;
			
    var jsonParams = {};
	
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	
	var itemData = $.ajax({
        type: "DELETE",
        url: session['context']+'/pos/'+saler,
        data: jsonParams,
        complete: function (xhr) {
        	$('#deleteModal').modal('hide');
        	$('#modal_button_confirm_clear_list').addClass('hide');
    		$('#modal_button_delete_item').addClass('hide');
        	search();
        	$('#add_form_item_barcode').focus();
        }
    }).done(function (){
        //close loader
    });
}

function showDataCustomer(code){
	var itemData = $.ajax({
        type: "GET",
        url: session['context']+'/customer/code/'+code,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText && xhr.responseText != '' ){
                		var customer = JSON.parse(xhr.responseText);
                		$('#payment_modal_button_customer_ok').fadeIn(300);
                		$("#btn_cutomer_view").attr('style',  'color: rgba(0,0,0,.84);font-size: 20px;background-color: #34f321;padding-left: 5px;padding-right: 2px;height: 42px;padding-top: 6px;width: 142px;');
                		$('#payment_modal_button_customer_add').hide();
                    	$('#customer_firstname').val('');
                    	$('#customer_lastname').val('');
                		$('#customer_add_firstname').hide();
                		$('#customer_add_lastname').hide();
                		
                	}else{
                		$('#payment_modal_button_customer_add').fadeIn(300);
                		$('#payment_modal_button_customer_ok').hide();
                		$("#btn_cutomer_view").attr('style',  'color: rgba(0,0,0,.84);font-size: 20px;background-color: #d2d2d2;padding-left: 5px;padding-right: 2px;height: 42px;padding-top: 6px;width: 142px;');
                		
                    	$('#customer_firstname').val('');
                    	$('#customer_lastname').val('');
                		$('#customer_add_firstname').hide();
                		$('#customer_add_lastname').hide();
                	}
                	
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function printSlip(soNumber,saleDate,saler,itemCount,subTotalAmount,rebateAmount,vatAmount,totalAmount,tendedAmount,changeAmount){
	$.ajax({
	        type: "GET",
	        headers: {
	            Accept: 'application/json'
	        },
	        url: session['context']+'/saleorderitem/findByCriteria?soId='+soNumber+'&firstResult=0&maxResult=1000',
	        complete: function (xhr2) {
	        	if(xhr2.responseText && xhr2.responseText != '' ){
            		 var jsonParams = {};
            			jsonParams['parentId']     = 1;
            			jsonParams[csrfParameter]  = csrfToken;
            		    jsonParams['item']         = xhr2.responseText;
            			
            		$.ajax({
    	                type: "POST",
    	    	        headers: {
    	    	            Accept: 'application/json'
    	    	        },
    	                url: 'http://printer/PrintPos/pos/printSlip/'+soNumber+'/'+saleDate+'/'+saler+'/'+itemCount+'/'+subTotalAmount+'/'+rebateAmount+'/'+vatAmount+'/'+totalAmount+'/'+tendedAmount+'/'+changeAmount,
    	                data:  jsonParams,
    	                complete: function (xhr) {
    	                	if (xhr.readyState == 4) {
    	                        if (xhr.status == 200) {
    	                        	//
    	                        }
    	                    }
    	                }
    	            }).done(function (){
    	                //close loader
    	            });
	        	}
	        	
	        	
	        	
	        }
	    }).done(function (){
	        //close loader
	});
	
	
}

function saveCustomer(){
	var jsonParams = {};
	
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	jsonParams['customerCode'] = $('#customer_code').val();
	jsonParams['firstname']    = $('#customer_firstname').val();
	jsonParams['lastname']     = $('#customer_lastname').val();
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/customer/save',
        data: jsonParams,
        complete: function (xhr) {
        	showDataCustomer($('#customer_code').val());
        }
    }).done(function (){
        //close loader
    });
}

function saveSo(){
	var saler = $("#app_username").html()==""?"null":$("#app_username").html()  ;
			
	var jsonParams = {};
	
	jsonParams['parentId']     			= 1;
	jsonParams[csrfParameter]  			= csrfToken;
	jsonParams['customerCode'] 			= $('#so_save_customerCode').val();
	jsonParams['saler']    				= saler;
	jsonParams['subTotalAmount']     	= $('#so_save_subTotalAmount').val();
	jsonParams['rebateAmount']     		= $('#so_save_rebateAmount').val();
	jsonParams['subTotalAfterRebateAmount'] = $('#so_save_subTotalAfterRebateAmount').val();
	jsonParams['vatAmount']     		= $('#so_save_vatAmount').val();
	jsonParams['subTotalAfterVatAmount']= $('#so_save_subTotalAfterVatAmount').val();
	jsonParams['totalAmount']     		= $('#so_save_totalAmount').val();
	jsonParams['tendedAmount']     		= $('#so_save_tendedAmount').val();
	jsonParams['changeAmount']     		= $('#so_save_changeAmount').val();
	jsonParams['itemCount']     		= $('#so_save_itemCount').val();
	jsonParams['roundType']     		= $('#so_save_roundType').val();
	jsonParams['paymentType']     		= $('#so_save_paymentType').val();
	jsonParams['itemCostAmount']     	= $('#so_save_itemCostAmount').val();
	jsonParams['fixCostPercentage']		= $('#so_save_fixCostPercentage').val();
	jsonParams['fixCostAmount']     	= $('#so_save_fixCostAmount').val();
	jsonParams['contributionPercentage']= $('#so_save_contributionPercentage').val();
	jsonParams['contributionAmount']	= $('#so_save_contributionAmount').val();
	jsonParams['marginPercentage']		= $('#so_save_marginPercentage').val();
	jsonParams['marginAmount']     		= $('#so_save_marginAmount').val();
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/pos/so/save',
        data: jsonParams,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText && xhr.responseText != '' ){
                		var saleOrderInformation = JSON.parse(xhr.responseText);
                		$('#invoice_modal_so').html(saleOrderInformation.soNumber);
                		$('#invoice_modal_saler').html(saleOrderInformation.saler);
                		$('#invoice_modal_sale_time').html(saleOrderInformation.saleDate);
                		$('#invoice_modal_total_item').html(saleOrderInformation.itemCount);
                	}
                }
        	}
        	
        	$('#paymentModal').modal('hide');
			$("#invoiceModal").modal({backdrop: "static"});
        }
    }).done(function (){
        //close loader
    });
}



function prepareEditQuantity(quantity,itemCode,itemBarcode){
	$('#quantity_modal_quantity').html(quantity);
	$('#quantity_modal_quantity_display').html(numberWithCommas(quantity));
	$('#quantity_modal_item_code').val(itemCode);
	$('#quantity_modal_item_barcode').val(itemBarcode);
	elementFocus = 'quantity_modal_quantity';
	editQuantityFirst = true;
}

function prepareDeleteItem(id,itemCode,itemBarcode,itemName){
	$('#modal_button_delete_item').removeClass('hide');
	$('#idDelete').val(id);

	$('#codeDelete').html(itemCode+' - '+b64_to_utf8(itemName));
	$('#labelDelete').html($MESSAGE_CONFIRM_TO_DELETE_ITEM);
}

function deleteItem(){
			
    var jsonParams = {};
	
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	
	var itemData = $.ajax({
        type: "DELETE",
        url: session['context']+'/pos/delete/'+$('#idDelete').val(),
        data: jsonParams,
        complete: function (xhr) {
        	$('#deleteModal').modal('hide');
        	$('#modal_button_confirm_clear_list').addClass('hide');
    		$('#modal_button_delete_item').addClass('hide');
        	search();
        	$('#add_form_item_barcode').focus();
        }
    }).done(function (){
        //close loader
    });
}


function search(){
	var criteriaObject = {
	        saler    : $("#app_username").html()==""?"null":$("#app_username").html()   
	    };
	queryByCriteria(criteriaObject);
	
}


function utf8_to_b64( str ) {
    return window.btoa(unescape(encodeURIComponent(str)));
}

function b64_to_utf8( str ) {
     return decodeURIComponent(escape(window.atob(str)));
}

function queryByCriteria(criteriaObject){
	object.setId("#paggingSearch");                
    object.setUrlData("/pos/findByCriteria"); 
    object.setUrlSize("/pos/findSize"); 
    
    object.setLimitData(1000);
    object.setDisplayPaging(false);
    
    object.loadTable = function(item){
        $('.dv-background').show();
        $('#gridSalesTransactionBody').empty();
        $dataForQuery = item;
        var itemCount = 0;
        var totalAmount = 0;
        var itemCostAmount = 0;
        for(var j=0;j<item.length;j++){
        	
            var itemCode = item[j].itemCode==null?"":item[j].itemCode;
            var itemBarcode = item[j].itemBarcode==null?"":item[j].itemBarcode;
            var itemName = item[j].itemName==null?"":item[j].itemName;
            var quantity = item[j].quantity==null?"":item[j].quantity;
            var sellingPriceAmount = item[j].sellingPriceAmount==null?"":item[j].sellingPriceAmount;
            var totalSellingPriceAmount = item[j].totalSellingPriceAmount==null?"":item[j].totalSellingPriceAmount;
            var totalPuechasePriceAmount = item[j].totalPuechasePriceAmount==null?"":item[j].totalPuechasePriceAmount;
            
            itemCount += quantity;
            totalAmount += totalSellingPriceAmount;
            itemCostAmount += totalPuechasePriceAmount;
            
            $('#gridSalesTransactionBody').append(''+
                    '<tr id="'+item[j].id+'" '+
                    ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                    '<td class="text-center">'+itemCode+'</td>'+
                    '<td class="text-center">'+itemBarcode+'</td>'+
                    '<td class="text-center">'+itemName+'</td>'+
                    '<td class="text-center">'+numberWithCommas(sellingPriceAmount)+'</td>'+
                    '<td class="text-center">'+numberWithCommas(quantity)+'</td>'+
                    '<td class="text-center">'+numberWithCommas(totalSellingPriceAmount)+'</td>'+
                    '<td class="text-center">'+
                    '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal" data-target="#editQuantityModal" onclick="prepareEditQuantity('+quantity+',\''+itemCode+'\',\''+itemBarcode+'\')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
    				'	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  data-toggle="modal" data-target="#deleteModal" onclick="prepareDeleteItem('+item[j].id+',\''+itemCode+'\',\''+itemBarcode+'\',\''+utf8_to_b64(itemName)+'\')" ><span class="fa fa-trash"><jsp:text/></span></button>'+
    				'</td>'+
                    '</tr>'
                );
        }
        
        //Item Cost
        $('#so_save_itemCostAmount').val(itemCostAmount );
        
        totalAmount = parseFloat(totalAmount).toFixed(2);
        
        $('#itemCount').html(itemCount);
        $('#itemCountDisplay').html(numberWithCommas(itemCount));
        $('#totalAmount').html(totalAmount);
        $('#totalAmountDisplay').html(numberWithCommas(totalAmount));
        $('#totalNet').html(totalAmount);
        $('#totalNetDisplay').html(numberWithCommas(totalAmount));
        
        
        calculateRebatePercentage();
        calculateRebateAmount();
        calculateVatAmount();
        
        $('#paggingSearch').addClass('hide');
        $("#invoiceModal").modal('hide');
        
        $('.dv-background').hide();
    };

    object.setDataSearch(criteriaObject); 

    object.search(object); 
}

function searchSaleOrder(customer){
	var criteriaObject = {
			customerCode    : customer			
	    };
	querySaleOrderByCriteria(criteriaObject);
}

function querySaleOrderByCriteria(criteriaObject){
	object2.setId("#paggingSearchSaleHistory");                
	object2.setUrlData("/saleorderitemcustomer/findByCriteria"); 
	object2.setUrlSize("/saleorderitemcustomer/findSize"); 
    
    
	object2.loadTable = function(item){
        $('.dv-background').show();
        $('#gridSaleHistoryBody').empty();
        $dataForQuery = item;
        for(var j=0;j<item.length;j++){
              
              var saler = item[j].createdBy==null?"":item[j].createdBy;
              var saleDate = item[j].createdDate==null?"":item[j].createdDate;
              var itemCode = item[j].itemCode==null?"":item[j].itemCode;
              var itemBarode = item[j].itemBarode==null?"":item[j].itemBarode;
              var itemName = item[j].itemName==null?"":item[j].itemName;
              var sellingPriceAmount = item[j].sellingPriceAmount==null?"":item[j].sellingPriceAmount;
              var quantity = item[j].quantity==null?"":item[j].quantity;
              var totalAmount = item[j].totalAmount==null?"":item[j].totalAmount;
              

            $('#gridSaleHistoryBody').append(''+
                    '<tr id="'+item[j].id+'" '+
                    ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                    '<td class="text-center" >'+saler+'</td>'+
                    '<td class="text-center" >'+saleDate+'</td>'+
                    '<td class="text-center" >'+itemCode+'</td>'+
                    '<td class="text-center" >'+itemBarode+'</td>'+
                    '<td class="text-center" >'+itemName+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(sellingPriceAmount)+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(quantity)+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(totalAmount)+'</td>'+
                    '</tr>'
                );
        }
        $('.dv-background').hide();
    };

    object2.setDataSearch(criteriaObject); 

    object2.search(object2); 
    


	$("#paggingSearchSaleHistoryLabel").attr('style',  'color: #03a9f4;');
	$('#customerHistoryModal').modal('show');
}

function calculateVatAmount(){
	if($('#vat_pct').val() && $('#totalNet').html()){
		var vat_pct = $('#vat_pct').val();
		var totalNet = $('#totalNet').html();
		var vat_amt = (vat_pct/100)*(totalNet*1);
		vat_amt = parseFloat(vat_amt).toFixed(2);
		$('#vat_amt').val(vat_amt);
		
		if($('#flagIncludeVat').val() == 'true'){
			var totalIncludeVat = (totalNet*1) + (vat_amt*1);
			totalIncludeVat = parseFloat(totalIncludeVat).toFixed(2);
			
			$('#totalNet').html(totalIncludeVat);
			$('#totalNetDisplay').html(numberWithCommas(totalIncludeVat));
		}
		
	}else {
		$('#totalNet').html($('#totalAfterRebate').val());
		$('#totalNetDisplay').html(numberWithCommas(parseFloat($('#totalAfterRebate').val()).toFixed(2)));
		$('#vat_amt').val('');
	}
}


function calculateRebateAmount(){
	if($('#rebate_amt').val() && $('#totalAmount').html()){
		var totalAmount = $('#totalAmount').html();
		var rebateAmount = $('#rebate_amt').val();
		var totalAfterRebate = (totalAmount*1) - (rebateAmount*1);
		totalAfterRebate = parseFloat(totalAfterRebate).toFixed(2);
		$('#totalNet').html(totalAfterRebate);
		$('#totalNetDisplay').html(numberWithCommas(totalAfterRebate));
		$('#totalAfterRebate').val(totalAfterRebate);
		
	}else{
		var totalAmount = $('#totalAmount').html();
		$('#totalNet').html(totalAmount);
		$('#totalNetDisplay').html(numberWithCommas(parseFloat(totalAmount).toFixed(2)));
		$('#totalAfterRebate').val(totalAmount);
	}
}

function calculateRebatePercentage(){
	if($('#rebate_pct').val() && $('#totalAmount').html()){
		var totalAmount = $('#totalAmount').html();
		var rebatePct = $('#rebate_pct').val();
		var rebateAmount = (rebatePct/100)*totalAmount;
		var totalAfterRebate = totalAmount - rebateAmount;
		totalAfterRebate = parseFloat(totalAfterRebate).toFixed(2);
		$('#totalNet').html(totalAfterRebate);
		$('#totalNetDisplay').html(numberWithCommas(totalAfterRebate));
		$('#totalAfterRebate').val(totalAfterRebate);
		
		rebateAmount = parseFloat(rebateAmount).toFixed(2);
		$('#rebate_amt').val(rebateAmount);
	}
}

function saveItem(){
	var saler =  $("#app_username").html()==""?"null":$("#app_username").html() ;
	
	var jsonParams = {};
	
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	jsonParams['itemCode']     = $('#add_form_item_code').val();
	jsonParams['itemBarcode']  = $('#add_form_item_barcode').val();
	jsonParams['quantity']     = $('#add_form_quantity').val();
	jsonParams['flagQuantity'] = 'P';
	
	jsonParams['saler']        = saler;
	
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/pos/save',
        data: jsonParams,
        complete: function (xhr) {
        	search();
        }
    }).done(function (){
        //close loader
    });
}

function updateQuantity(){
	var saler =  $("#app_username").html()==""?"null":$("#app_username").html() ;
	
	var jsonParams = {};
	
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	jsonParams['itemCode']     = $('#quantity_modal_item_code').val();
	jsonParams['itemBarcode']  = $('#quantity_modal_item_barcode').val();
	jsonParams['quantity']     = $('#quantity_modal_quantity').html();
	jsonParams['saler']        = saler;
	
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/pos/save',
        data: jsonParams,
        complete: function (xhr) {
        	$('#editQuantityModal').modal('hide');
        	search();
        	$('#add_form_item_barcode').focus();
            
        }
    }).done(function (){
        //close loader
    });
}



function saveItemAndClearForm(){
	var saler =  $("#app_username").html()==""?"null":$("#app_username").html() ;
	
	var jsonParams = {};
	
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	jsonParams['itemCode']     = $('#add_form_item_code').val();
	jsonParams['itemBarcode']  = $('#add_form_item_barcode').val();
	jsonParams['quantity']     = $('#add_form_quantity').val();
	jsonParams['flagQuantity'] = 'P';
	jsonParams['saler']        = saler;
	
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/pos/save',
        data: jsonParams,
        complete: function (xhr) {
        	pressed = false;
        	search();
        	$('#add_form_item_code').val('');
        	$('#add_form_item_barcode').val('');
        	$('#add_form_quantity').val('');
        	$('#add_form_item_barcode').focus();
        }
    }).done(function (){
        //close loader
    });
}

function roundTenUp(){

	$('#so_save_roundType').val('TU');
	var balance_amount = $('#payment_modal_balance_amount_fact').html();
	balance_amount = Math.ceil(balance_amount/10) * 10;
	balance_amount = parseFloat(balance_amount).toFixed(2);
	$('#payment_modal_balance_amount_value').html(balance_amount);
	$('#payment_modal_balance_amount').html(numberWithCommas(balance_amount));
	$('#revertStartPrice').fadeIn(300);
}


function roundTenDown(){
	$('#so_save_roundType').val('TD');
	var balance_amount = $('#payment_modal_balance_amount_fact').html();
	balance_amount = Math.floor(balance_amount/10) * 10;
	balance_amount = parseFloat(balance_amount).toFixed(2);
	$('#payment_modal_balance_amount_value').html(balance_amount);
	$('#payment_modal_balance_amount').html(numberWithCommas(balance_amount));
	$('#revertStartPrice').fadeIn(300);
}



function roundHundredUp(){
	$('#so_save_roundType').val('HU');
	var balance_amount = $('#payment_modal_balance_amount_fact').html();
	balance_amount = Math.ceil(balance_amount/100) * 100;
	balance_amount = parseFloat(balance_amount).toFixed(2);
	$('#payment_modal_balance_amount_value').html(balance_amount);
	$('#payment_modal_balance_amount').html(numberWithCommas(balance_amount));
	$('#revertStartPrice').fadeIn(300);
}


function roundHundredUDown(){

	$('#so_save_roundType').val('HD');
	var balance_amount = $('#payment_modal_balance_amount_fact').html();
	balance_amount = Math.floor(balance_amount/100) * 100;
	balance_amount = parseFloat(balance_amount).toFixed(2);
	$('#payment_modal_balance_amount_value').html(balance_amount);
	$('#payment_modal_balance_amount').html(numberWithCommas(balance_amount));
	$('#revertStartPrice').fadeIn(300);
}

function resetTended(){
	$('#so_save_roundType').val('');
	$('#payment_modal_tended_amount').html('0.00');
	$('#payment_modal_tended_amount_fact').html('0.00');
	$('#flag_tended_keyvalue').val('true');
}

function plusValue(val){
	var tended_amount = $('#payment_modal_tended_amount_fact').html();
	tended_amount = (tended_amount * 1) + val;
	tended_amount = parseFloat(tended_amount).toFixed(2);
	$('#payment_modal_tended_amount_fact').html(tended_amount);
	$('#payment_modal_tended_amount').html(numberWithCommas(tended_amount));
}

function keyValue(val){
	var value = $('#payment_modal_tended_amount_fact').html();
	if($('#flag_tended_keyvalue').val() == 'true'){
		value = '';
		$('#flag_tended_keyvalue').val('false');
	}
	
	value +=  val;
	$('#payment_modal_tended_amount_fact').html(value);
	$('#payment_modal_tended_amount').html(numberWithCommas(value));
	
}

function applyKeyboardTo(){
	var el = $('#'+elementFocus);
	var valueTo = '';
	if (el[0].value !== undefined) {
		valueTo = el.val();
	}else{
		valueTo = el.html();
	}
	
	var et = $('#'+applyTo);
	if (et[0].value !== undefined) {
		et.val(valueTo).change();
	}else{
		et.html(valueTo);
	}
	
}

function keyValueInput(val){
	var el = $('#'+elementFocus);
	if (el[0].value !== undefined) {
		var value = el.val();
		value +=  val;
	    el.val(value).change();
	} else {
		var value = el.html();
		value +=  val;
	    el.html(value);
	}
	$('#'+elementFocus).focus();
	
}

function clearCurrent(){
	var el = $('#'+elementFocus);
	if (el[0].value !== undefined) {
	    el.val('').change();
	} else {
	    el.html('');
	}
	$('#'+elementFocus).focus();
}

function keyValueInputOfNumber(val){
	var el = $('#'+elementFocus);
	if (el[0].value !== undefined) {
		var value = el.val();
		
		if(value=='0' || editQuantityFirst){ value = ''; }
		
		value +=  val;
	    el.val(value).change();
	    $('#'+elementFocus+'_display').val(numberWithCommas(value)).change();
	    editQuantityFirst = false;
	} else {
		var value = el.html();
		if(value=='0'|| editQuantityFirst){ value = ''; }
		
		value +=  val;
	    el.html(value);
	    $('#'+elementFocus+'_display').html(numberWithCommas(value));
	    editQuantityFirst = false;
	}
	
}

function clearCurrentOfNumber(){
	var el = $('#'+elementFocus);
	if (el[0].value !== undefined) {
	    el.val('0');
	    $('#'+elementFocus+'_display').val('0');
	} else {
	    el.html('0');
	    $('#'+elementFocus+'_display').html('0');
	}
}



function selectPaymentType(e){
	$('.activePayType').addClass('pay-type');
	$('.activePayType').removeClass('activePayType');
	$('#'+e).addClass('activePayType');
	$('#'+e).removeClass('pay-type');
	$('#so_save_paymentType').val(e);
}

function callSaveItemFromBarcodeScanner(){
	setTimeout(function(){
        // check we have a long length e.g. it is a barcode
		if($('#add_form_item_barcode').val().length >= 13){
			saveItemAndClearForm();
			pressed = false;
		}else{
			callSaveItemFromBarcodeScanner();
		}
		
    },220);
}

function calculateFinalVat(){
	var balance_amount = $('#payment_modal_balance_amount_value').html()*1;
	var vat_pct = $('#vat_pct').val()*1;
	var vat_amt = $('#vat_amt').val()*1;
	
	var total_amount_final_exclude_vat = 0.0;
	if($('#vat_pct').val()){
		total_amount_final_exclude_vat = (balance_amount*100)/(100+vat_pct);
	}else{
		total_amount_final_exclude_vat = balance_amount - vat_amt ;
	}
	
	
	var final_vat_amt = balance_amount - total_amount_final_exclude_vat;
	return final_vat_amt;
	
}



var timeDifKeyPress = 0.0;
var pressed = false; 
var elementFocus = '';
var applyTo = '';
var editQuantityFirst = true;

var stompClient = null;

function connect() {
	
    var socket = new SockJS('/StockPlus/barcodescan');
    stompClient = Stomp.over(socket);  
    
    stompClient.connect({}, function(frame) {
        
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/barcodescan/messages', function(messageOutput) {
        	var messageObj = JSON.parse(messageOutput.body);
        	$('#add_form_item_barcode').val(messageObj.text);
        	saveItemAndClearForm();
        });
    });
}

$(document).ready(function () {
	
	
	$('#add_form_item_barcode').focus();
	elementFocus = 'add_form_item_barcode';
	
	

	$('#add_form_item_barcode').on("change",function(e){
		if ($('#add_form_item_barcode').val().length >= 13 && pressed==false) {
			pressed = true;
			
			saveItemAndClearForm();
		}
    });
	
	
	
	$('#add_form_item_barcode').on("keyup",function(e){
		var difTime = e.originalEvent.timeStamp - timeDifKeyPress;
		
		timeDifKeyPress = e.originalEvent.timeStamp;
		
		//difTime < 40 && 
		if ($('#add_form_item_barcode').val().length >= 13 && pressed==false) {
			//Use for case more EAN13
			/*if(pressed == false){
				console.log("before callSaveItemFromBarcodeScanner");
				callSaveItemFromBarcodeScanner();
			}
			*/
			pressed = true;
			
			saveItemAndClearForm();
		}
		
		
    });
	
	/*$("#add_form_item_barcode").keypress(function(e){
		console.log("Prevent form submit.="+e.originalEvent.timeStamp);
	    if ( e.which === 13 ) {
	        console.log("Prevent 13");
	        e.preventDefault();
	    	saveItemAndClearForm();
	    }
	});*/
	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#rebate_amt').on("change",function(e){
		$('#rebate_pct').val('');
		calculateRebatePercentage();
        calculateRebateAmount();
		calculateVatAmount();
		
    });
	
	$('#rebate_pct').on("change",function(e){
		calculateRebatePercentage();
        calculateRebateAmount();
		calculateVatAmount();
		
    });
	
	$('#vat_pct').on("change",function(e){
		calculateRebatePercentage();
        calculateRebateAmount();
		calculateVatAmount();
    });
	
	
	
	$('#button_cancel').on("click",function(e){
		$('#codeDelete').html('');
		$('#labelDelete').html($MESSAGE_CONFIRM_TO_CLEAR_ITEM);
		$('#modal_button_confirm_clear_list').removeClass('hide');
		$('#deleteModal').modal('show');
    });
	
	$('#button_payment').on("click",function(e){
		if( $('#itemCount').html() =='0' || $('#itemCount').html() =='-'){
			$('#warningModal .modal-body').html($MESSAGE_ITEM_IS_EMPTY);
			$('#warningModal').modal('show');
		}else{
			$('#payment_modal_balance_amount').html(numberWithCommas($('#totalNet').html()));
			$('#payment_modal_balance_amount_fact').html($('#totalNet').html());
			$('#payment_modal_balance_amount_value').html($('#totalNet').html());
			
			
			$('#payment_modal_button_customer_add').fadeOut(1);
    		$('#payment_modal_button_customer_add').fadeOut(1);
    		
			$('#paymentModal').modal('show');
			$('#customer_modal_customer').html($('#customer_code').val());
			$('#customer_code').val('');
			elementFocus = 'customer_modal_customer';
			$('#so_save_paymentType').val( 'pay_cash');
		}
		
    });
	
	$('#revertStartPrice').on("click",function(e){
		$('#payment_modal_balance_amount').html(numberWithCommas($('#payment_modal_balance_amount_fact').html()));
		$('#payment_modal_balance_amount_value').html($('#payment_modal_balance_amount_fact').html());
		$('#revertStartPrice').fadeOut(300);
	});
	
	
	
	$('#button_accept_money').on("click",function(e){
		$('#button_accept_money_disable').removeClass('hide');
		$('#button_cancel_accept_money_disable').removeClass('hide');
		$('#button_accept_money').addClass('hide');
		$('#button_cancel_accept_money').addClass('hide');
		
		
		var balance_amount_value = $('#payment_modal_balance_amount_value').html();
		balance_amount_value = balance_amount_value*1;
		balance_amount_value = parseFloat(balance_amount_value).toFixed(2);
		
		var tended_amount_value = $('#payment_modal_tended_amount_fact').html();
		tended_amount_value = tended_amount_value*1;
		tended_amount_value = parseFloat(tended_amount_value).toFixed(2);
		
		var change_amount_fact = tended_amount_value - balance_amount_value;
		change_amount_fact = parseFloat(change_amount_fact).toFixed(2);
		
		var final_vat_amount = parseFloat(calculateFinalVat()).toFixed(2);
		var final_subtotal_amount = parseFloat($('#totalAmount').html()).toFixed(2);
		
		var final_rebate_amount = final_subtotal_amount - (balance_amount_value - final_vat_amount);
		final_rebate_amount = parseFloat(final_rebate_amount).toFixed(2);
		
		var rebate_amount_acc = final_subtotal_amount - final_rebate_amount;
		rebate_amount_acc = parseFloat(rebate_amount_acc).toFixed(2);
		
		var vat_amount_acc = (final_subtotal_amount - final_rebate_amount) + (final_vat_amount*1);
		vat_amount_acc = parseFloat(vat_amount_acc).toFixed(2);
		
		var fixCostPercentage = $('#so_save_fixCostPercentage').val() / 100;
		
		var fixCostAmount = parseFloat((fixCostPercentage * rebate_amount_acc)).toFixed(2); 
		
		var itemCostAmount = $('#so_save_itemCostAmount').val( ) * 1;
		var contributionAmount = rebate_amount_acc - itemCostAmount;
		contributionAmount = parseFloat(contributionAmount).toFixed(2); 
		
		var contributionPercentage = ((rebate_amount_acc - itemCostAmount)/rebate_amount_acc) * 100;
		contributionPercentage = parseFloat(contributionPercentage).toFixed(2); 
		

		var marginAmount = rebate_amount_acc - itemCostAmount - fixCostAmount;
		marginAmount = parseFloat(marginAmount).toFixed(2); 
		
		var marginPercentage = ((rebate_amount_acc - itemCostAmount - fixCostAmount)/rebate_amount_acc) * 100;
		marginPercentage = parseFloat(marginPercentage).toFixed(2); 
		
		if(change_amount_fact >= 0){
			$('#invoice_modal_change_amount').html(numberWithCommas(change_amount_fact));
			$('#invoice_modal_change_amount_display').html(numberWithCommas(change_amount_fact));
			$('#invoice_modal_total_amount').html(numberWithCommas(balance_amount_value));
			$('#invoice_modal_tended_amount').html(numberWithCommas(tended_amount_value));
			$('#invoice_modal_subtotal_amount').html(numberWithCommas(final_subtotal_amount));
			$('#invoice_modal_rebate_amount').html(numberWithCommas(final_rebate_amount));
			$('#invoice_modal_rebate_amount_acc').html(numberWithCommas(rebate_amount_acc));
			$('#invoice_modal_vat_amount').html(numberWithCommas(final_vat_amount));
			$('#invoice_modal_vat_amount_acc').html(numberWithCommas( vat_amount_acc));
			
			$('#invoice_modal_total_item').html(numberWithCommas( $('#itemCount').html()));
			

			$('#so_save_customerCode').val($('#customer_code').val());
			$('#so_save_subTotalAmount').val( final_subtotal_amount);
			$('#so_save_rebateAmount').val(final_rebate_amount );
			$('#so_save_subTotalAfterRebateAmount').val( rebate_amount_acc);
			$('#so_save_vatAmount').val( final_vat_amount);
			$('#so_save_subTotalAfterVatAmount').val( vat_amount_acc);
			$('#so_save_tendedAmount').val(tended_amount_value );
			$('#so_save_changeAmount').val(change_amount_fact );
			$('#so_save_totalAmount').val(balance_amount_value );

			$('#so_save_itemCount').val($('#itemCount').html() );
			$('#so_save_fixCostAmount').val( fixCostAmount);
			$('#so_save_contributionPercentage').val( contributionPercentage);
			$('#so_save_contributionAmount').val( contributionAmount);
			$('#so_save_marginPercentage').val( marginPercentage);
			$('#so_save_marginAmount').val( marginAmount);
			
			saveSo();
			
		}else{
			$('#warningModal .modal-body').html($MESSAGE_MONEY_NOT_ENOUGH+" <label style='font-size: 60px;'>"+numberWithCommas(change_amount_fact*(-1))+" </label><span class='fa fa-btc'></span>");
			$('#warningModal').modal('show');
		}
		
		
    });
	
	$('#button_invoice_complete').on("click",function(e){
		printSlip(
				$('#invoice_modal_so').html(),
				$('#invoice_modal_sale_time').html(),
				$('#invoice_modal_saler').html(),
				$('#invoice_modal_total_item').html(),
				$('#invoice_modal_subtotal_amount').html(),
				$('#invoice_modal_rebate_amount').html(),
				$('#invoice_modal_vat_amount').html(),
				$('#invoice_modal_total_amount').html(),
				$('#invoice_modal_tended_amount').html(),
				$('#invoice_modal_change_amount').html()
				
		) ;
		clearPosList();
		
    });
	
	
	
	
	$('#modal_button_confirm_clear_list').on("click",function(e){
		clearPosList();
		
    });
	
	$('#modal_button_delete_item').on("click",function(e){
		deleteItem();
		
    });
	
	
	
	

	$('#modal_button_edit_quantity_save').on("click",function(e){
		updateQuantity();
		
    });
	
	
	$('#editQuantityModal').on('hidden.bs.modal', function () {
		$('#quantity_modal_item_code').val('');
		$('#quantity_modal_item_barcode').val('');
		$('#quantity_modal_quantity').html('0');
		$('#quantity_modal_quantity_display').html('0');
		$('#add_form_item_barcode').focus();
	});
	
	
	$('#deleteModal').on('hidden.bs.modal', function () {
		$('#modal_button_confirm_clear_list').addClass('hide');
		$('#modal_button_delete_item').addClass('hide');
		$('#add_form_item_barcode').focus();
	});
	
	//$('#customer_code').on("click",function(e){
	//	applyTo = 'customer_code';
	//	$('#customer_modal_customer').html($('#customer_code').val());
	//	$('#customerModal').modal('show');
    //});
	
	$('#customer_view').on("click",function(e){
		if('rgb(52, 243, 33)' == $('#btn_cutomer_view').css('backgroundColor')){
			searchSaleOrder($('#customer_code').val());
		}
		
    });
	
	$('#customer_code').on("change",function(e){
		//Check customer
		showDataCustomer($('#customer_code').val());
    });
	
	$('#payment_modal_button_customer_save').on("click",function(e){
		//validate Save Customer
		var errorMessage = "";
		
		if(!$('#customer_code').val()){
			errorMessage += $MESSAGE_REQUIRE_CUSTOMER_CODE+"<br/>";
		}
		if(!$('#customer_firstname').val()){
			errorMessage += $MESSAGE_REQUIRE_CUSTOMER_FIRST_NAME+"<br/>";
		}
		if(!$('#customer_lastname').val()){
			errorMessage += $MESSAGE_REQUIRE_CUSTOMER_LASTNAME+"<br/>";
		}
		
		
		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			saveCustomer();
		}
		
		
    });
	
	
	
	$('#payment_modal_button_customer_add').on("click",function(e){
		$('#payment_modal_button_customer_add').hide();
		$('#customer_add_firstname').show();
		$('#customer_add_lastname').show();
    });
	
	
	
	$('#invoiceModal').on('hidden.bs.modal', function () {
		$('#invoice_modal_change_amount_display').html('0.00');
		$('#invoice_modal_subtotal_amount').html('0.00');
		$('#invoice_modal_rebate_amount_acc').html('0.00');
		$('#invoice_modal_vat_amount_acc').html('0.00');
		$('#invoice_modal_total_amount').html('0.00');
		$('#invoice_modal_tended_amount').html('0.00');
		$('#invoice_modal_change_amount').html('0.00');
		$('#invoice_modal_rebate_amount').html('0.00');
		$('#invoice_modal_vat_amount').html('0.00');
		

		$('#invoice_modal_so').html('XXXXXX');
		$('#invoice_modal_saler').html('-');
		$('#invoice_modal_sale_time').html('-');
		$('#invoice_modal_total_item').html('-');
		

		$('#vat_pct').val('');
		$('#rebate_pct').val('');
		
		
		$('#so_save_customerCode').val('');
		$('#so_save_subTotalAmount').val( '');
		$('#so_save_rebateAmount').val('' );
		$('#so_save_subTotalAfterRebateAmount').val( '');
		$('#so_save_vatAmount').val( '');
		$('#so_save_subTotalAfterVatAmount').val( '');
		$('#so_save_tendedAmount').val('' );
		$('#so_save_changeAmount').val('' );
		$('#so_save_totalAmount').val('' );

		$('#so_save_itemCount').val('' );
		$('#so_save_roundType').val('' );
		$('#so_save_paymentType').val( '');
		$('#so_save_itemCostAmount').val( '');
		$('#so_save_fixCostPercentage').val( '12.5');
		$('#so_save_fixCostAmount').val('' );
		$('#so_save_contributionPercentage').val('' );
		$('#so_save_contributionAmount').val( '');
		$('#so_save_marginPercentage').val('' );
		$('#so_save_marginAmount').val('' );
		$('#add_form_item_barcode').focus();
	});
	
	
	
	
	$('#paymentModal').on('hidden.bs.modal', function () {
		

		$('#button_accept_money_disable').addClass('hide');
		$('#button_cancel_accept_money_disable').addClass('hide');
		$('#button_accept_money').removeClass('hide');
		$('#button_cancel_accept_money').removeClass('hide');
		
		
		$('#payment_modal_balance_amount').html('0.00');
		$('#payment_modal_balance_amount_fact').html('0.00');
		$('#payment_modal_tended_amount').html('0.00');
		$('#payment_modal_tended_amount_fact').html('0.00');
		$('#payment_modal_balance_amount_value').html('0.00');
		$('#revertStartPrice').fadeOut(300);

		$('#customer_code').val('');
		$('#customer_firstname').val('');
		$('#customer_lastname').val('');
		$('#customer_add_firstname').hide();
		$('#customer_add_lastname').hide();
		$('#payment_modal_button_customer_ok').hide();
		$("#btn_cutomer_view").attr('style',  'color: rgba(0,0,0,.84);font-size: 20px;background-color: #d2d2d2;padding-left: 5px;padding-right: 2px;height: 42px;padding-top: 6px;width: 142px;');
		$('#payment_modal_button_customer_add').hide();
		
		selectPaymentType('pay_cash');
	})
	
	$('#add_form_item_barcode,#add_form_item_code,#add_form_quantity,#rebate_amt,#rebate_pct,#vat_pct').on('focus', function (e) {
		elementFocus = $(this).attr('id');
	});
	
	
	
	search();
	//For Option POS Mobile
	//setInterval(function(){ search();}, 3000);
	connect();
	
	
	
	
});

