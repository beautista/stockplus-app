/**
 * 
 */

var object = $.extend({},UtilPagination);

var object2 = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function showDataItem(item_id){
	$("tr").removeClass("trblue");
	
	$('#detail_soNumber').html($('#'+item_id+'_soNumber').html());
	$('#detail_saler').html($('#'+item_id+'_saler').html());
	$('#detail_saleDate').html($('#'+item_id+'_saleDate').html());
	$('#detail_totalAmount').html(   $('#'+item_id+'_totalAmount').val());
	$('#detail_itemCount').html(   $('#'+item_id+'_itemCount').val());
	$('#detail_rebateAmount').html(   $('#'+item_id+'_rebateAmount').val());
	$('#detail_vatAmount').html(   $('#'+item_id+'_vatAmount').val());
	$('#detail_customerCode').html(   $('#'+item_id+'_customerCode').val());
	
    $('#detail_'+'subTotalAmount').html(   $('#'+item_id+'_'+'subTotalAmount').val());
    $('#detail_'+'subTotalAfterRebateAmount').html(   $('#'+item_id+'_'+'subTotalAfterRebateAmount').val());
    $('#detail_'+'subTotalAfterVatAmount').html(   $('#'+item_id+'_'+'subTotalAfterVatAmount').val());
    $('#detail_'+'tendedAmount').html(   $('#'+item_id+'_'+'tendedAmount').val());
    $('#detail_'+'changeAmount').html(   $('#'+item_id+'_'+'changeAmount').val());
    $('#detail_'+'roundType').html(   $('#'+item_id+'_'+'roundType').val());
    $('#detail_'+'paymentType').html(   $('#'+item_id+'_'+'paymentType').val());
    $('#detail_'+'itemCostAmount').html(   $('#'+item_id+'_'+'itemCostAmount').val());
    $('#detail_'+'fixCostPercentage').html(   $('#'+item_id+'_'+'fixCostPercentage').val());
    $('#detail_'+'fixCostAmount').html(   $('#'+item_id+'_'+'fixCostAmount').val());
    $('#detail_'+'contributionPercentage').html(   $('#'+item_id+'_'+'contributionPercentage').val());
    $('#detail_'+'contributionAmount').html(   $('#'+item_id+'_'+'contributionAmount').val());
    $('#detail_'+'marginPercentage').html(   $('#'+item_id+'_'+'marginPercentage').val());
    $('#detail_'+'marginAmount').html(   $('#'+item_id+'_'+'marginAmount').val());
    

	$('#detail_x_totalAmount').html(   $('#'+item_id+'_totalAmount').val());
	$('#detail_x_rebateAmount').html(   $('#'+item_id+'_rebateAmount').val());
	$('#detail_x_vatAmount').html(   $('#'+item_id+'_vatAmount').val());
	
	
	
	
	searchOrderItem(item_id);
	$('#'+item_id).addClass('trblue');
	
}

function searchOrderItem(soId){
	var criteriaObject = {
			soId    : soId			
	    };
	querySaleOrderItemByCriteria(criteriaObject);
}

function querySaleOrderItemByCriteria(criteriaObject){
	object2.setId("#paggingSearchSaleOrderItem");                
	object2.setUrlData("/saleorderitem/findByCriteria"); 
	object2.setUrlSize("/saleorderitem/findSize"); 
    
	object2.setLimitData(1000);
	object2.setDisplayPaging(false);
    
	object2.loadTable = function(item){
        $('.dv-background').show();
        $('#gridSaleOrderItemBody').empty();
        $dataForQuery = item;
        for(var j=0;j<item.length;j++){

        	
            var itemCode = item[j].itemCode==null?"":item[j].itemCode;
            var itemBarode = item[j].itemBarode==null?"":item[j].itemBarode;
            var itemName = item[j].itemName==null?"":item[j].itemName;
            var sellingPriceAmount = item[j].sellingPriceAmount==null?"":item[j].sellingPriceAmount;
            var quantity = item[j].quantity==null?"":item[j].quantity;
            var totalAmount = item[j].totalAmount==null?"":item[j].totalAmount;
            
            $('#gridSaleOrderItemBody').append(''+
                    '<tr id="'+item[j].id+'" '+
                    ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                    '<td class="text-center" >'+itemCode+'</td>'+
                    '<td class="text-center" >'+itemBarode+'</td>'+
                    '<td class="text-center" >'+itemName+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(sellingPriceAmount)+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(quantity)+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(totalAmount)+'</td>'+
                    '</tr>'
                );
        }
        $('.dv-background').hide();
    };

    object2.setDataSearch(criteriaObject); 

    object2.search(object2); 
}

function searchOrder(){
	var criteriaObject = {
			soNumber    : $("#search_soNumber").val()==""?"null":$("#search_soNumber").val(),
			saleDateString    : $("#search_saleDate").val()==""?"null":$("#search_saleDate").val(),
			customerCode    : $("#search_customerCode").val()==""?"null":$("#search_customerCode").val()
							
	    };
	querySaleOrderByCriteria(criteriaObject);
}

function querySaleOrderByCriteria(criteriaObject){
	object.setId("#paggingSearchSaleOrder");                
    object.setUrlData("/saleorder/findByCriteria"); 
    object.setUrlSize("/saleorder/findSize"); 
    
    object.loadTable = function(item){
        $('.dv-background').show();
        $('#gridSaleOrderBody').empty();
        $dataForQuery = item;
        for(var j=0;j<item.length;j++){
            var soNumber = item[j].soNumber==null?"":item[j].soNumber;
            var saler = item[j].saler==null?"":item[j].saler;
            var saleDate = item[j].saleDate==null?"":item[j].saleDate;
            var customerCode = item[j].customerCode==null?"":item[j].customerCode;
            var totalAmount = item[j].totalAmount==null?"":item[j].totalAmount;
            var itemCount = item[j].itemCount==null?"":item[j].itemCount;
            var rebateAmount = item[j].rebateAmount==null?"":item[j].rebateAmount;
            var vatAmount = item[j].vatAmount==null?"":item[j].vatAmount;
            

            var subTotalAmount = item[j].subTotalAmount==null?"":item[j].subTotalAmount;
            var subTotalAfterRebateAmount = item[j].subTotalAfterRebateAmount==null?"":item[j].subTotalAfterRebateAmount;
            var subTotalAfterVatAmount = item[j].subTotalAfterVatAmount==null?"":item[j].subTotalAfterVatAmount;
            var tendedAmount = item[j].tendedAmount==null?"":item[j].tendedAmount;
            var changeAmount = item[j].changeAmount==null?"":item[j].changeAmount;
            var roundType = item[j].roundType==null?"":item[j].roundType;
            var paymentType = item[j].paymentType==null?"":item[j].paymentType;
            var itemCostAmount = item[j].itemCostAmount==null?"":item[j].itemCostAmount;
            var fixCostPercentage = item[j].fixCostPercentage==null?"":item[j].fixCostPercentage;
            var fixCostAmount = item[j].fixCostAmount==null?"":item[j].fixCostAmount;
            var contributionPercentage = item[j].contributionPercentage==null?"":item[j].contributionPercentage;
            var contributionAmount = item[j].contributionAmount==null?"":item[j].contributionAmount;
            var marginPercentage = item[j].marginPercentage==null?"":item[j].marginPercentage;
            var marginAmount = item[j].marginAmount==null?"":item[j].marginAmount;
            
            
            
            $('#gridSaleOrderBody').append(''+
                    '<tr id="'+item[j].id+'" '+
                    ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                    '<td class="text-center" id="'+item[j].id+'_soNumber" >'+soNumber+'</td>'+
                    '<td class="text-center" id="'+item[j].id+'_saler" >'+saler+'</td>'+
                    '<td class="text-center" id="'+item[j].id+'_saleDate">'+saleDate+'</td>'+
                    '<td class="text-center"  >'+numberWithCommas(totalAmount)+'</td>'+
                    '<td class="text-center">'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'totalAmount" value="'+numberWithCommas(parseFloat(totalAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'itemCount" value="'+itemCount  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'rebateAmount" value="'+numberWithCommas(parseFloat(rebateAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'vatAmount" value="'+numberWithCommas(parseFloat(vatAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'subTotalAmount" value="'+numberWithCommas(parseFloat(subTotalAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'subTotalAfterRebateAmount" value="'+numberWithCommas(parseFloat(subTotalAfterRebateAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'subTotalAfterVatAmount" value="'+numberWithCommas(parseFloat(subTotalAfterVatAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'tendedAmount" value="'+numberWithCommas(parseFloat(tendedAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'changeAmount" value="'+numberWithCommas(parseFloat(changeAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'roundType" value="'+roundType  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'paymentType" value="'+paymentType  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'itemCostAmount" value="'+numberWithCommas(parseFloat(itemCostAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'fixCostPercentage" value="'+numberWithCommas(parseFloat(fixCostPercentage).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'fixCostAmount" value="'+numberWithCommas(parseFloat(fixCostAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'contributionPercentage" value="'+numberWithCommas(parseFloat(contributionPercentage).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'contributionAmount" value="'+numberWithCommas(parseFloat(contributionAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'marginPercentage" value="'+numberWithCommas(parseFloat(marginPercentage).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'marginAmount" value="'+numberWithCommas(parseFloat(marginAmount).toFixed(2))  +'"  />'+
                    '   <input type="hidden" id="'+item[j].id+'_'+'customerCode" value="'+customerCode  +'"  />'+
                    
                    '	<button type="button" class="btn btn-material-cyan-500  btn-style-small" title="View" onclick="showDataItem('+item[j].id+')" ><span class="fa fa-arrow-right"><jsp:text/></span></button>'+
    				'</td>'+
                    '</tr>'
                );
        }
        $('.dv-background').hide();
    };

    object.setDataSearch(criteriaObject); 

    object.search(object); 
}









$(document).ready(function () {
	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_button').on('click', function () {
		searchOrder();
	});
	
	
	
	$('#special_information_button').on('click', function () {
		if($('#special_information').is(":visible")){
			$('#special_information').fadeOut(0);
			$('#special_information_button').addClass('top0').removeClass('top200');
			$('#special_information_button span').addClass('fa-eye').removeClass('fa-eye-slash');
			
		}else{
			$('#special_information').fadeIn(700);
			$('#special_information_button').addClass('top200').removeClass('top0');
			$('#special_information_button span').addClass('fa-eye-slash').removeClass('fa-eye');
		}
		
		
	});

	
	
	
	searchOrder();
	
});

