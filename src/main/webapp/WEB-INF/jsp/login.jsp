<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

<link rel="stylesheet" type="text/css"
	href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- 
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
<c:url value="/css/main.css" var="jstlCss" />
<link href="${jstlCss}" rel="stylesheet" />

</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">iSpectro Web</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="/">Home</a></li>
					<li><a href="/product">Product</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">
		<h3>Login <span class="label label-default" id="modeCurrent" >AUTO</span></h3>
		<button type="button" class="btn btn-primary" id="autoButton" >Set AUTO</button>
		<button type="button" class="btn btn-warning" id="manualButton" >Set Manual</button>
		<br><br>
		<div class="form-group row">
		  <div class="col-xs-1">
		    <label for="ex3">Product Code</label>
		  </div>
		  <div class="col-xs-3">
		    <input class="form-control" id="productCode" type="text">
		  </div>
		  <div class="col-xs-8">
		    <button type="button" class="btn btn-primary"   id="learn3Button" >Learn Data</button>
		    <button type="button" class="btn btn-warning"   id="clear3Learn" >Clear Learn</button>
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-1">
		    <label for="ex3">Station Number</label>
		  </div>
		  <div class="col-xs-3">
		    <input class="form-control" id="stationNumber" type="text">
		  </div>
		  <div class="col-xs-8">
		    <button type="button" class="btn btn-primary"   id="learn2Button" >Learn Data</button>
		    <button type="button" class="btn btn-warning"   id="clear2Learn" >Clear Learn</button>
		  </div>
		</div>
        <h3>Action</h3>
		<button type="button" class="btn btn-success"  id="runButton" >Run</button>
		<button type="button" class="btn btn-primary"   id="learnButton" >Learn Data</button>
		<button type="button" class="btn btn-warning" id="clearButton" >Clear Form</button>
		<button type="button" class="btn btn-warning"   id="clearLearn" >Clear Learn</button>
		<br><br>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">FE - (Iron)</label>
		    <input class="form-control" id="0" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">C - (Carbon)</label>
		    <input class="form-control" id="1" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Mn - (Manganese)</label>
		    <input class="form-control" id="2" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Si - (Silicon)</label>
		    <input class="form-control" id="3" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">P - (Phosphorus)</label>
		    <input class="form-control" id="4" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">S - (Sulfur)</label>
		    <input class="form-control" id="5" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Ni - (Nickel)</label>
		    <input class="form-control" id="6" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Cr - (Chromuim)</label>
		    <input class="form-control" id="7" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Cu - (Copper)</label>
		    <input class="form-control" id="8" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Mo - (Molybdenum)</label>
		    <input class="form-control" id="9" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">V - (Vanadium)</label>
		    <input class="form-control" id="10" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Ti - (Titanium)</label>
		    <input class="form-control" id="11" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Al - (Aluminium)</label>
		    <input class="form-control" id="12" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Nb - (Niobium)</label>
		    <input class="form-control" id="13" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">W - (Tungsten)</label>
		    <input class="form-control" id="14" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">As - (Arsenic)</label>
		    <input class="form-control" id="15" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Sn - (Tin)</label>
		    <input class="form-control" id="16" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Co - (Cobalt)</label>
		    <input class="form-control" id="17" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Pb - (Lead)</label>
		    <input class="form-control" id="18" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Sb - (Boron)</label>
		    <input class="form-control" id="19" type="text">
		  </div>
		</div><div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Sb - (Antimony)</label>
		    <input class="form-control" id="20" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Bi - (Bismuth)</label>
		    <input class="form-control" id="21" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Ca - (Calcium)</label>
		    <input class="form-control" id="22" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Mg - (Magnesium)</label>
		    <input class="form-control" id="23" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">Zn - (Zinc)</label>
		    <input class="form-control" id="24" type="text">
		  </div>
		</div>
		<div class="form-group row">
		  <div class="col-xs-4">
		    <label for="ex3">N - (Nitrogen)</label>
		    <input class="form-control" id="25" type="text">
		  </div>
		</div>
		

	</div>
	<!-- /.container -->

	<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="/webjars/jquery/1.11.1/jquery.min.js"></script>
	
	<script type="text/javascript">
	$(document).ready(function(){
	    
		$("#autoButton").click(function(){
					$.post("/setMode",
				    {
				        command: "MODE AUTO"
				    },
				    function(data, status){
				    	$("#modeCurrent").text(data);
				    });
		});
		

		$("#manualButton").click(function(){
					$.post("/setMode",
				    {
				        command: "MODE MANUAL"
				    },
				    function(data, status){
				    	$("#modeCurrent").text(data);
				    });
		});
		
		$("#runButton").click(function(){
			var dataObj = {
			        command: "RUN",
			        0:$("#0").val(),
			        1:$("#1").val(),
			        2:$("#2").val(),
			        3:$("#3").val(),
			        4:$("#4").val(),
			        5:$("#5").val(),
			        6:$("#6").val(),
			        7:$("#7").val(),
			        8:$("#8").val(),
			        9:$("#9").val(),
			        10:$("#10").val(),
			        11:$("#11").val(),
			        12:$("#12").val(),
			        13:$("#13").val(),
			        14:$("#14").val(),
			        15:$("#15").val(),
			        16:$("#16").val(),
			        17:$("#17").val(),
			        18:$("#18").val(),
			        19:$("#19").val(),
			        20:$("#20").val(),
			        21:$("#21").val(),
			        22:$("#22").val(),
			        23:$("#23").val(),
			        24:$("#24").val(),
			        25:$("#25").val(),
			        
			    };
			
			$.post("/action",
		    {
				dataObj : JSON.stringify(dataObj)
		    },
		    function(data, status){
		    	//alert(data);
		    	//$("#modeCurrent").text(data);
		    });
			
	 });
		
		$("#learnButton").click(function(){
			var dataObj = {
			        command: "LEARN DATA1",
			        0:$("#0").val(),
			        1:$("#1").val(),
			        2:$("#2").val(),
			        3:$("#3").val(),
			        4:$("#4").val(),
			        5:$("#5").val(),
			        6:$("#6").val(),
			        7:$("#7").val(),
			        8:$("#8").val(),
			        9:$("#9").val(),
			        10:$("#10").val(),
			        11:$("#11").val(),
			        12:$("#12").val(),
			        13:$("#13").val(),
			        14:$("#14").val(),
			        15:$("#15").val(),
			        16:$("#16").val(),
			        17:$("#17").val(),
			        18:$("#18").val(),
			        19:$("#19").val(),
			        20:$("#20").val(),
			        21:$("#21").val(),
			        22:$("#22").val(),
			        23:$("#23").val(),
			        24:$("#24").val(),
			        25:$("#25").val(),
			        
			    };
			
			$.post("/action",
		    {
				dataObj : JSON.stringify(dataObj)
		    },
		    function(data, status){
		    	//$("#modeCurrent").text(data);
		    });
		});
		
		
		
		$("#0").blur(function(){
			var data = $("#0").val().split(" ");
			$("#0").val(data[0]);
	        $("#1").val(data[1]);
	        $("#2").val(data[2]);
	        $("#3").val(data[3]);
	        $("#4").val(data[4]);
	        $("#5").val(data[5]);
	        $("#6").val(data[6]);
	        $("#7").val(data[7]);
	        $("#8").val(data[8]);
	        $("#9").val(data[9]);
	        $("#10").val(data[10]);
	        $("#11").val(data[11]);
	        $("#12").val(data[12]);
	        $("#13").val(data[13]);
	        $("#14").val(data[14]);
	        $("#15").val(data[15]);
	        $("#16").val(data[16]);
	        $("#17").val(data[17]);
	        $("#18").val(data[18]);
	        $("#19").val(data[19]);
	        $("#20").val(data[20]);
	        $("#21").val(data[21]);
	        $("#22").val(data[22]);
	        $("#23").val(data[23]);
	        $("#24").val(data[24]);
	        $("#25").val(data[25]);
		});
		
		$("#clearButton").click(function(){
			$("#0").val("");
	        $("#1").val("");
	        $("#2").val("");
	        $("#3").val("");
	        $("#4").val("");
	        $("#5").val("");
	        $("#6").val("");
	        $("#7").val("");
	        $("#8").val("");
	        $("#9").val("");
	        $("#10").val("");
	        $("#11").val("");
	        $("#12").val("");
	        $("#13").val("");
	        $("#14").val("");
	        $("#15").val("");
	        $("#16").val("");
	        $("#17").val("");
	        $("#18").val("");
	        $("#19").val("");
	        $("#20").val("");
	        $("#21").val("");
	        $("#22").val("");
	        $("#23").val("");
	        $("#24").val("");
	        $("#25").val("");
		});
		
		$("#clearLearn").click(function(){
			var dataObj = {
			        command: "CLEAR LEARN1",
			        0:$("#0").val(),
			        1:$("#1").val(),
			        2:$("#2").val(),
			        3:$("#3").val(),
			        4:$("#4").val(),
			        5:$("#5").val(),
			        6:$("#6").val(),
			        7:$("#7").val(),
			        8:$("#8").val(),
			        9:$("#9").val(),
			        10:$("#10").val(),
			        11:$("#11").val(),
			        12:$("#12").val(),
			        13:$("#13").val(),
			        14:$("#14").val(),
			        15:$("#15").val(),
			        16:$("#16").val(),
			        17:$("#17").val(),
			        18:$("#18").val(),
			        19:$("#19").val(),
			        20:$("#20").val(),
			        21:$("#21").val(),
			        22:$("#22").val(),
			        23:$("#23").val(),
			        24:$("#24").val(),
			        25:$("#25").val(),
			        
			    };
			
			$.post("/action",
		    {
				dataObj : JSON.stringify(dataObj)
		    },
		    function(data, status){
		    	//$("#modeCurrent").text(data);
		    });
			
	 	});
		
		$("#learn2Button").click(function(){
			var dataObj = {
			        command: "LEARN DATA2",
			        stationNumber:$("#stationNumber").val()
			        
			    };
			
			$.post("/action",
		    {
				dataObj : JSON.stringify(dataObj)
		    },
		    function(data, status){
		    	//$("#modeCurrent").text(data);
		    });
			
	 	});
		
		$("#clear2Learn").click(function(){
			var dataObj = {
			        command: "CLEAR LEARN2",
			        stationNumber:$("#stationNumber").val()
			        
			    };
			
			$.post("/action",
		    {
				dataObj : JSON.stringify(dataObj)
		    },
		    function(data, status){
		    	//$("#modeCurrent").text(data);
		    });
			
	 	});
		
		$("#learn3Button").click(function(){
			var dataObj = {
			        command: "LEARN DATA3",
			        productCode:$("#productCode").val()
			        
			    };
			
			$.post("/action",
		    {
				dataObj : JSON.stringify(dataObj)
		    },
		    function(data, status){
		    	//$("#modeCurrent").text(data);
		    });
			
	 	});
		
		$("#clear3Learn").click(function(){
			var dataObj = {
			        command: "CLEAR LEARN3",
			        productCode:$("#productCode").val()
			        
			    };
			
			$.post("/action",
		    {
				dataObj : JSON.stringify(dataObj)
		    },
		    function(data, status){
		    	//$("#modeCurrent").text(data);
		    });
			
	 	});
		
		
	});
	
	</script>

</body>

</html>
